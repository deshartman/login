package com.wl.test.login.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface RegisterFailEventHandler extends EventHandler {
		void onRegister(RegisterFailEvent event);
}
