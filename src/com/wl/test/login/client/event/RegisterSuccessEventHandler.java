package com.wl.test.login.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface RegisterSuccessEventHandler extends EventHandler {
	void onRegister(RegisterSuccessEvent event);
}
