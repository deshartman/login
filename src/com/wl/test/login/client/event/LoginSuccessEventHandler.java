package com.wl.test.login.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface LoginSuccessEventHandler extends EventHandler {
	void onLogin(LoginSuccessEvent event);
}
