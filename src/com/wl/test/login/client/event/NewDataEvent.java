package com.wl.test.login.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class NewDataEvent extends GwtEvent<NewDataEventHandler> {

	public static Type<NewDataEventHandler> TYPE = new Type<NewDataEventHandler>();
	
	private final String jsonData;

	public NewDataEvent(String jsonData) {
		super();
		this.jsonData = jsonData;
	}

	public String getJsonData() {
		return jsonData;
	}

	@Override
	public Type<NewDataEventHandler> getAssociatedType() {
	    return TYPE;
	}
	
	@Override
	protected void dispatch(NewDataEventHandler handler) {
		handler.onDataReturned(this);
	}

}
