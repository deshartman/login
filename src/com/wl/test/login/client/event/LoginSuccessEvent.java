package com.wl.test.login.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class LoginSuccessEvent extends GwtEvent<LoginSuccessEventHandler> {
	
	
	 public static Type<LoginSuccessEventHandler> TYPE = new Type<LoginSuccessEventHandler>();
	  
	  @Override
	  public Type<LoginSuccessEventHandler> getAssociatedType() {
	    return TYPE;
	  }

	  @Override
	  protected void dispatch(LoginSuccessEventHandler handler) {
	    handler.onLogin(this);
	  }
}
