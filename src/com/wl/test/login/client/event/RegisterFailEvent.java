package com.wl.test.login.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class RegisterFailEvent extends GwtEvent<RegisterFailEventHandler> {

	public static Type<RegisterFailEventHandler> TYPE = new Type<RegisterFailEventHandler>();

	private final String message;

	public RegisterFailEvent(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	@Override
	public Type<RegisterFailEventHandler> getAssociatedType() {
		// TODO Auto-generated method stub
		return TYPE;
	}

	@Override
	protected void dispatch(RegisterFailEventHandler handler) {
		handler.onRegister(this);
	}

}
