package com.wl.test.login.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface NewDataEventHandler extends EventHandler {
	void onDataReturned(NewDataEvent event);

}
