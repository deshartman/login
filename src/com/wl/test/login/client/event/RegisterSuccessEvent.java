package com.wl.test.login.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class RegisterSuccessEvent extends GwtEvent<RegisterSuccessEventHandler> {

	@Override
	public Type<RegisterSuccessEventHandler> getAssociatedType() {
		// TODO Auto-generated method stub
		return TYPE;
	}

	 public static Type<RegisterSuccessEventHandler> TYPE = new Type<RegisterSuccessEventHandler>();
	  

	  @Override
	  protected void dispatch(RegisterSuccessEventHandler handler) {
	    handler.onRegister(this);
	  }

}
