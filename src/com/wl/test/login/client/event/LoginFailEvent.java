package com.wl.test.login.client.event;

import com.google.gwt.event.shared.GwtEvent;

public class LoginFailEvent extends GwtEvent<LoginFailEventHandler> {

	public static Type<LoginFailEventHandler> TYPE = new Type<LoginFailEventHandler>();

	private final String message;

	public LoginFailEvent(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	@Override
	public Type<LoginFailEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(LoginFailEventHandler handler) {
		handler.onLogin(this);
	}

}
