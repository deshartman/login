package com.wl.test.login.client.event;

import com.google.gwt.event.shared.EventHandler;

public interface LoginFailEventHandler extends EventHandler {
	void onLogin(LoginFailEvent event);
}
