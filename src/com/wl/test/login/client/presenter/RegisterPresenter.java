package com.wl.test.login.client.presenter;

import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.wl.test.login.client.delegate.AuthenticationDelegateImpl;
import com.wl.test.login.client.event.RegisterFailEvent;
import com.wl.test.login.client.event.RegisterFailEventHandler;
import com.wl.test.login.client.event.RegisterSuccessEvent;
import com.wl.test.login.client.event.RegisterSuccessEventHandler;
import com.wl.test.login.shared.Globals;

public class RegisterPresenter implements Presenter {

	// Presenter Display Interface
	public interface Display {
		// Create a hook for this presenter or inject
		void setPresenter(RegisterPresenter presenter);

		void showWarning(String error); // Show that there is an error

		void setResponse(String response);

		void setFaultResponse(String fault);

		Widget asWidget();
	}

//	private String serverResponse;
	protected AuthenticationDelegateImpl authenticationDelegate = AuthenticationDelegateImpl.getInstance();
	private SimpleEventBus eventBus;
	private Display display;

	// ** Constructor **//
	public RegisterPresenter(Display display, SimpleEventBus eventBus) {
		super();
		this.eventBus = eventBus;
		this.display = display;
		bind();
	}

	/**
	 * Bind Event Handlers to there Handlers
	 */
	private void bind() {
		eventBus.addHandler(RegisterSuccessEvent.TYPE, new RegisterSuccessEventHandler() {
			public void onRegister(RegisterSuccessEvent event) {
				goCancel();
			}
		});

		eventBus.addHandler(RegisterFailEvent.TYPE, new RegisterFailEventHandler() {
			public void onRegister(RegisterFailEvent event) {
				display.setFaultResponse(event.getMessage());
			}
		});
	}

	public void register(String username, String password, String retype, String mobile) {
		// Check that the passwords match, else complain
		if (password.equals(retype)) {
			// Passwords match, so register
			authenticationDelegate.callRegister(username, password, this.eventBus);
		} else {
			// Passwords mismatch, warn user
			display.showWarning("Passwords do not match, please retry!");
		}

	}

	public void goCancel() {
		// Go back to whatever brought you here (Pops "register" off the stack)
		// History.back();
		History.newItem(Globals.LOGIN_URL);

	}

	@Override
	public void go(HasWidgets container) {
		display.setPresenter(this);

		container.clear();
		container.add(display.asWidget());

	}

}
