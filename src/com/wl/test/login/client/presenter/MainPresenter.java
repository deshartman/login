package com.wl.test.login.client.presenter;

import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.wl.test.login.client.delegate.AuthenticationDelegateImpl;
import com.wl.test.login.client.delegate.ServiceDelegateImpl;
import com.wl.test.login.client.event.LoginFailEvent;
import com.wl.test.login.client.event.LoginFailEventHandler;
import com.wl.test.login.client.event.LoginSuccessEvent;
import com.wl.test.login.client.event.LoginSuccessEventHandler;
import com.wl.test.login.client.event.NewDataEvent;
import com.wl.test.login.client.event.NewDataEventHandler;
import com.wl.test.login.shared.Globals;

public class MainPresenter implements Presenter {

	// Presenter Display Interface
	public interface Display {
		// Create a hook for this presenter or inject
		void setPresenter(MainPresenter presenter);
		
		void showWarning(String error);	// Show that there is an error
		
		void setResponse(String response);
		void setFaultResponse(String fault);
		
		Widget asWidget();
	}
	
	private String serverResponse;
	protected ServiceDelegateImpl serviceDelegate = ServiceDelegateImpl.getInstance();
	private SimpleEventBus eventBus;
	private Display display;
	

	//** Constructor **//
	public MainPresenter( Display display, SimpleEventBus eventBus) {
		super();
		this.eventBus = eventBus;
		this.display = display;
		bind();
	}

	/**
	 * Bind Event Handlers to there Handlers
	 */
	private void bind() {

		eventBus.addHandler(NewDataEvent.TYPE, new NewDataEventHandler() {
			@Override
			public void onDataReturned(NewDataEvent event) {
				display.setResponse(event.getJsonData());
			}
		});

	}
	
	/**
	 * Call JSON service and retrieve the array of data to display
	 */
	public void getData() {
		serviceDelegate.getData("ABC+DEF", this.eventBus);
	}
	
	public void goCancel() {
		// Go back to whatever brought you here (Pops "register" off the stack)
		//History.back();
		History.newItem(Globals.LOGIN_URL);
		
	}
	
	@Override
	public void go(HasWidgets container) {
		display.setPresenter(this);
		
		container.clear();
	    container.add(display.asWidget());

	}

}
