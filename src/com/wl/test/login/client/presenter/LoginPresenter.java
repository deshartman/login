package com.wl.test.login.client.presenter;

import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.wl.test.login.client.delegate.AuthenticationDelegateImpl;
import com.wl.test.login.client.event.LoginFailEvent;
import com.wl.test.login.client.event.LoginFailEventHandler;
import com.wl.test.login.client.event.LoginSuccessEvent;
import com.wl.test.login.client.event.LoginSuccessEventHandler;
import com.wl.test.login.shared.Globals;

public class LoginPresenter implements Presenter {

	// Presenter Display Interface
	public interface Display {
		// Create a hook for this presenter or inject
		void setPresenter(LoginPresenter presenter);

		void showUsername(String username); // Show the username if
											// "Remember Me" is clicked

		void showError(String error); // Show that there is an error

		void setResponse(String response);

		void setFaultResponse(String fault);

		Widget asWidget();
	}

	protected AuthenticationDelegateImpl authenticationDelegate = AuthenticationDelegateImpl.getInstance();
	
	private SimpleEventBus eventBus;
	private Display display;

	// ** Constructor **//
	public LoginPresenter(Display display, SimpleEventBus eventBus) {
		super();
		this.eventBus = eventBus;
		this.display = display;
		bind();
	}

	/**
	 * Bind Event Handlers to there Handlers
	 */
	private void bind() {

		eventBus.addHandler(LoginSuccessEvent.TYPE, new LoginSuccessEventHandler() {
			public void onLogin(LoginSuccessEvent event) {
				gotoMain();
			}
		});

		eventBus.addHandler(LoginFailEvent.TYPE, new LoginFailEventHandler() {
			public void onLogin(LoginFailEvent event) {
				display.setFaultResponse(event.getMessage());
			}
		});

		
	}

	/**
	 * Log in using the supplied username and password
	 * 
	 * @param username
	 * @param password
	 */
	public void login(String username, String password) {
		boolean error = false;
		String errorMessage = "";
		// CharSequence chars = new CharSequence()

		// Check that you have a valid username
		if (username.length() < Globals.MIN_PASSWORD_LENGTH) {
			if (errorMessage.length() > 0) {
				errorMessage = errorMessage + " & ";
			}
			errorMessage += "Username too short";
			error = true;
		}

		// Check that you have a valid password
		if (password.length() < Globals.MIN_PASSWORD_LENGTH) {
			if (errorMessage.length() > 0) {
				errorMessage = errorMessage + " & ";
			}
			errorMessage += "Password too short";
			error = true;
		}

		if (error) {
			display.showError(errorMessage);
		} else {
			authenticationDelegate.callLogin(username, password, this.eventBus);

		}
	}

	public void gotoRegister() {
		// Navigate using History URLs
		History.newItem(Globals.REGISTER_URL);
	}

	public void gotoMain() {
		History.newItem(Globals.MAIN_URL);
	}

	@Override
	public void go(HasWidgets container) {
		display.setPresenter(this);

		container.clear();
		container.add(display.asWidget());
	}

	// **************************************** EVENTS **************************************
}
