package com.wl.test.login.client.view;

import com.google.gwt.core.client.GWT;
import com.wl.test.login.client.presenter.MainPresenter;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.HTML;

public class MainViewImpl extends Composite implements GWTView, MainPresenter.Display {

	private static MainViewImplUiBinder uiBinder = GWT.create(MainViewImplUiBinder.class);
	@UiField Button btnGetData;
	@UiField HTML htmlResponse;
	
	private MainPresenter presenter;

	interface MainViewImplUiBinder extends UiBinder<Widget, MainViewImpl> {
	}

	public MainViewImpl()  {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public void setPresenter(MainPresenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public void showWarning(String error) {
		htmlResponse.setHTML(error);
		
	}

	@Override
	public void setResponse(String response) {
		htmlResponse.setHTML(response);
		
	}

	@Override
	public void setFaultResponse(String fault) {
		htmlResponse.setHTML(fault);
		
	}

	
	@UiHandler("btnGetData")
	void onBtnGetDataClick(ClickEvent event) {
		presenter.getData();
	}
}
