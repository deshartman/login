package com.wl.test.login.client.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.wl.test.login.client.presenter.RegisterPresenter;

public class RegisterViewImpl extends Composite implements GWTView, RegisterPresenter.Display {

	private static RegisterViewImplUiBinder uiBinder = GWT.create(RegisterViewImplUiBinder.class);
	interface RegisterViewImplUiBinder extends UiBinder<Widget, RegisterViewImpl> {}

	@UiField Button btnRegister;
	@UiField TextBox txtUsername;
	@UiField TextBox txtPassword;
	@UiField TextBox txtRetype;
	@UiField TextBox txtMobile;
	
	@UiField Label txtWarningMessage;
	@UiField HTML htmlResponse;
	@UiField Button btnCancel;
	
	private RegisterPresenter presenter;


	public RegisterViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public void setPresenter(RegisterPresenter presenter) {
		this.presenter = presenter;
		
	}

	@Override
	public void showWarning(String error) {
		txtWarningMessage.setText(error);
		
	}

	@Override
	public void setResponse(String response) {
		htmlResponse.setHTML(response);
		
	}
	
	@UiHandler("btnCancel")
	void onBtnCancelClick(ClickEvent event) {
		presenter.goCancel();
	}

	@UiHandler("btnRegister")
	void onBtnRegisterClick(ClickEvent event) {
		presenter.register(txtUsername.getText(), txtPassword.getText(), txtRetype.getText(), txtMobile.getText());
	}

	@Override
	public void setFaultResponse(String fault) {
		// TODO Auto-generated method stub
		
	}
}
