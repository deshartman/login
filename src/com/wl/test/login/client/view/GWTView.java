package com.wl.test.login.client.view;

import com.google.gwt.user.client.ui.IsWidget;

/**
 * 
 * @author des
 * 
 *         A key concept of MVP development is that a view is defined by an
 *         interface. This allows multiple view implementations based on client
 *         characteristics (such as mobile vs. desktop) and also facilitates
 *         lightweight unit testing by avoiding the time-consuming GWTTestCase.
 *         There is no View interface or class in GWT which views must implement
 *         or extend; however, GWT 2.1 introduces an IsWidget interface that is
 *         implemented by most Widgets as well as Composite. It is useful for
 *         views to extend IsWidget if they do in fact provide a Widget.
 */

public interface GWTView extends IsWidget {
	
}
