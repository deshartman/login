package com.wl.test.login.client.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.wl.test.login.client.presenter.LoginPresenter;

/**
 * 
 * @author des
 * 
 *         The Presenter interface and setPresenter method allow for
 *         bi-directional communication between view and presenter, which
 *         simplifies interactions involving repeating Widgets and also allows
 *         view implementations to use UiBinder with @UiHandler methods that
 *         delegate to the presenter interface.
 * 
 *         The HelloView implementation uses UiBinder and a template.
 */

public class LoginViewImpl extends Composite implements GWTView, LoginPresenter.Display {

	// Tie this java owner class to the UI xml. This is only needed if the names
	// are different.
	@UiTemplate("LoginViewImpl.ui.xml")
	interface LoginViewUiBinder extends UiBinder<Widget, LoginViewImpl> {
	}

	private static LoginViewUiBinder uiBinder = GWT.create(LoginViewUiBinder.class);

	@UiField
	TextBox txtUsername;
	@UiField
	TextBox txtPassword;
	@UiField
	Button btnLogin;
	@UiField
	Label txtWarningMessage;
	@UiField
	HTML htmlResponse;
	@UiField Hyperlink hlRegister;

	private LoginPresenter presenter;

	// Constructor
	public LoginViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public void setPresenter(LoginPresenter presenter) {
		this.presenter = presenter;

	}

	@Override
	public void showUsername(String username) {
		// TODO Auto-generated method stub

	}

	@UiHandler("btnLogin")
	void onBtnLoginClick(ClickEvent event) {
		presenter.login(txtUsername.getText(), txtPassword.getText());

	}

	@Override
	public void setResponse(String response) {
		htmlResponse.setHTML(response);
	}

	@Override
	public void showError(String error) {
		txtWarningMessage.setText(error);
	}

	@Override
	public void setFaultResponse(String fault) {
		txtWarningMessage.setText(fault);
		
	}

	@UiHandler("hlRegister")
	void onHlRegisterClick(ClickEvent event) {
		// This puts a new History token on th stack and thus changes the view.
		// Does not need an event fired
		
		//presenter.gotoRegister();
	}
}
