package com.wl.test.login.client.controller;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.HasWidgets;
import com.wl.test.login.client.presenter.LoginPresenter;
import com.wl.test.login.client.presenter.MainPresenter;
import com.wl.test.login.client.presenter.Presenter;
import com.wl.test.login.client.presenter.RegisterPresenter;
import com.wl.test.login.client.view.LoginViewImpl;
import com.wl.test.login.client.view.MainViewImpl;
import com.wl.test.login.client.view.RegisterViewImpl;
import com.wl.test.login.shared.Globals;

public class AppController implements Presenter, ValueChangeHandler<String> {

	private final SimpleEventBus eventBus;
	private HasWidgets container;

	public AppController() {
		this.eventBus = new SimpleEventBus();
		History.addValueChangeHandler(this);
	}

	/**
	 * This is the entry point for this class and is executed when all setup is
	 * done. This takes the user to login by default
	 * 
	 */
	@Override
	public void go(HasWidgets container) {
		this.container = container;

		if (History.getToken().length() > 0) {
			History.fireCurrentHistoryState();
		} else {
			History.newItem("login");
		}
	}

	/**
	 * This changes to the correct view based on the history token currently on
	 * the stack. This is used when clicking the forward and back buttons in the
	 * browser or when the token has changed. History will call this method if
	 * it has changed
	 */
	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		String token = event.getValue();

		if (token != null) {
			Presenter presenter = null;

			if (token.equals(Globals.LOGIN_URL)) {
				presenter = new LoginPresenter(new LoginViewImpl(), eventBus);
			} else if (token.equals(Globals.REGISTER_URL)) {
				presenter = new RegisterPresenter(new RegisterViewImpl(), eventBus);

			} else if (token.equals(Globals.MAIN_URL)) {
				presenter = new MainPresenter(new MainViewImpl(), eventBus);
			}

			if (presenter != null) {
				presenter.go(container);
			}

		}
	}

}
