package com.wl.test.login.client.delegate;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.http.client.URL;
import com.wl.test.login.client.event.NewDataEvent;

/**
 * This delegate is used to receive HTTP traffic from the server. It is JSON
 * based and will be parsed once received
 * 
 * @author des
 * 
 */
public class ServiceDelegateImpl implements ServiceDelegate {

	private static ServiceDelegateImpl instance = new ServiceDelegateImpl();

	private ServiceDelegateImpl() {
	}

	public static ServiceDelegateImpl getInstance() {
		return instance;
	}

	/**
	 * Get the JSON String from the server based on the the requested URL
	 * @return
	 */
	public void getData(String url, final SimpleEventBus eventbus) {

		String new_url = GWT.getModuleBaseURL() + "data?q=" + url;
		
		// Need to add some checks here for the URL
		
		RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, URL.encode(new_url));

		try {
			Request request = builder.sendRequest(null, new RequestCallback() {

				public void onResponseReceived(Request request, Response response) {
					if (response.getStatusCode() == 200) {
						// Process the response in response.getText()
						// Fire Event to process the response
						eventbus.fireEvent(new NewDataEvent(response.getText()));
					} else {
						// Handle the error. Can get the status text from
						// response.getStatusText()
						// Fire Event to process the Error
						eventbus.fireEvent(new NewDataEvent(response.getStatusText()));
					}
				}

				public void onError(Request request, Throwable exception) {
					// Couldn't connect to server (could be timeout, SOP
					// violation, etc.)
					// Fire Event to process the Error
					eventbus.fireEvent(new NewDataEvent(exception.getMessage()));
				}

			});
		} catch (RequestException e) {
			// Couldn't connect to server
			// Fire Event to process the Error
		}

	}

}
