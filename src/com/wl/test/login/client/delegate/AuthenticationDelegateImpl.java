package com.wl.test.login.client.delegate;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.wl.test.login.client.event.LoginFailEvent;
import com.wl.test.login.client.event.LoginSuccessEvent;
import com.wl.test.login.client.event.RegisterFailEvent;
import com.wl.test.login.client.event.RegisterSuccessEvent;
import com.wl.test.login.shared.AuthenticationService;
import com.wl.test.login.shared.AuthenticationServiceAsync;
import com.wl.test.login.shared.data.Authentication;

public class AuthenticationDelegateImpl implements AuthenticationDelegate {
	
	private static AuthenticationDelegateImpl instance = new AuthenticationDelegateImpl();
	
	// Create a remote service proxy to talk to the server-side Greeting service.
	private final AuthenticationServiceAsync authService = GWT.create(AuthenticationService.class);

	private AuthenticationDelegateImpl() {
	}

	public static AuthenticationDelegateImpl getInstance() {
		return instance;
	}

	
	public void callLogin(String username, String password, final SimpleEventBus eventbus) {
		
		authService.login(username, password, new AsyncCallback<Authentication>() {
			public void onSuccess(Authentication authentication) {
				if(authentication.proceed){
					eventbus.fireEvent(new LoginSuccessEvent());
				} else {
					eventbus.fireEvent(new LoginFailEvent(authentication.errorMessage));
				}
			}
			
			public void onFailure(Throwable caught) {
				eventbus.fireEvent(new LoginFailEvent(caught.getMessage()));
			}
		});
	}
	
	public void callRegister(String username, String password, final SimpleEventBus eventBus) {

		authService.register(username, password, new AsyncCallback<Authentication>() {
			public void onSuccess(Authentication authentication) {
				eventBus.fireEvent(new RegisterSuccessEvent());
			}
			
			public void onFailure(Throwable caught) {
				eventBus.fireEvent(new RegisterFailEvent(caught.getMessage()));
			}
		});
	}

	

}
