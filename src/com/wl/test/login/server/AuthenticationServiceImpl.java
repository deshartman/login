package com.wl.test.login.server;

import org.springframework.stereotype.Service;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.wl.test.login.shared.AuthenticationService;
import com.wl.test.login.shared.data.Authentication;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
@Service("authService")
public class AuthenticationServiceImpl extends RemoteServiceServlet implements AuthenticationService { 
	
	@Override
	public Authentication login(String username, String password) {
		Authentication authentication = new Authentication();
		
		// Mock at the moment
		if(username.equals("deshartman") && password.equals("deshartman")){
			authentication.proceed = true;
			authentication.message = "Welcome back!!";
		} else {
			authentication.proceed = false;
			authentication.errorMessage = "Bad credentials";
		}
		
		return authentication;
	}

	@Override
	public Authentication register(String username, String password) {
		Authentication authentication = new Authentication();
		
		// mock registration
		authentication.proceed = true;
		authentication.message = "Welcome " + username;
		
		return authentication;
	}

}
