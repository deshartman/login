package com.wl.test.login.shared;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.wl.test.login.shared.data.Authentication;

public interface AuthenticationServiceAsync {

	void login(String username, String password, AsyncCallback<Authentication> callback);
	void register(String username, String password, AsyncCallback<Authentication> callback);

}
