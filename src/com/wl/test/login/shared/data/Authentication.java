package com.wl.test.login.shared.data;

import com.google.gwt.user.client.rpc.IsSerializable;

public class Authentication implements IsSerializable {
	public Boolean proceed;
	public String message;
	public String errorMessage;
	public String warningMessage;
	public String faultMessage;

}
