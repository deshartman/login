package com.wl.test.login.shared;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.wl.test.login.shared.data.Authentication;

@RemoteServiceRelativePath("Login/authService")
public interface AuthenticationService extends RemoteService {
	Authentication login(String username, String password);
	Authentication register(String username, String password);

}
