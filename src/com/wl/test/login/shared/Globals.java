package com.wl.test.login.shared;

public class Globals {
	
	
	public static final int MIN_PASSWORD_LENGTH = 6;
	public static final String LOGIN_URL = "login";
	public static final String REGISTER_URL = "register";
	public static final String MAIN_URL = "main";

}
