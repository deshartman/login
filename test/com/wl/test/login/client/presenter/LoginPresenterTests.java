package com.wl.test.login.client.presenter;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyZeroInteractions;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.gwt.event.shared.SimpleEventBus;
import com.wl.test.login.shared.AuthenticationServiceAsync;

public class LoginPresenterTests {

	private LoginPresenter longinPresenter;
	private SimpleEventBus eventBus;
	private AuthenticationServiceAsync mockRpcService;
	private LoginPresenter.Display mockDisplay;

	@Before
	public void setUp() throws Exception {
		mockRpcService = mock(AuthenticationServiceAsync.class);
		eventBus = new SimpleEventBus();
		mockDisplay = mock(LoginPresenter.Display.class);
		longinPresenter = new LoginPresenter(mockDisplay, eventBus);
	}

	@After
	public void tearDown() throws Exception {
		mockRpcService = null;
		eventBus = null;
		mockDisplay = null;
		longinPresenter = null;
	}

	@Test
	/**
	 * Make sure the service is not called with invalid usernames or passwords
	 */
	public void testLogin_short_username() {
		String username = "AA";
		String password = "LONGENOUGH";
		
		longinPresenter.login(username, password);
		
		// Ensure the RPC is not called
//		verify( mockRpcService, never() );
		
		verifyZeroInteractions(mockRpcService);
	}
	
	
	@Test
	/**
	 * Make sure the service is not called with invalid usernames or passwords
	 */
	public void testLogin_short_password() {
		String username = "LONGENOUGH";
		String password = "AA";
		
		longinPresenter.login(username, password);
		
		// Ensure the RPC is not called
		verifyZeroInteractions(mockRpcService);
	}

	@Test
	/**
	 * Cannot really Mock this, as History is GWT specific.
	 */
	public void testGotoRegister() {
		//longinPresenter.gotoRegister();
		//assertTrue(History.getToken().equals(Globals.REGISTER_URL));

	}

	

}







	
	
	
	